package com.example.e.objects;

public class Places {
    private String latitude;
    private String longtitude;
    private City city;
    private String nameText;
    private String imageURL;

    public String getNameText() {
        return nameText;
    }

    public void setNameText(String nameText) {
        this.nameText = nameText;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(String longtitude) {
        this.longtitude = longtitude;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;

    }
}
