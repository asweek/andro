package com.example.e.objects;

/**
 * Created by Danila on 24.08.2014.
 */
public class City {
    private String cityName;

    public City() {

    }

    public City(String name) {
        this.cityName = name;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
