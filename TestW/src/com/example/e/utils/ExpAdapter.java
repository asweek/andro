package com.example.e.utils;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorTreeAdapter;
import android.widget.TextView;
import com.example.e.R;

/**
 * Created by Danila on 07.09.2014.
 */
public class ExpAdapter extends CursorTreeAdapter {
    private LayoutInflater mInflator;
    private DatabaseHelper databaseHelper;

    public ExpAdapter(Cursor cursor, Context context) {
        super(cursor, context);
        this.mInflator = LayoutInflater.from(context);
        this.databaseHelper = new DatabaseHelper(context);

    }

    @Override
    protected void bindChildView(View view, Context context, Cursor cursor,
                                 boolean isLastChild) {
        TextView tvChild = (TextView) view.findViewById(R.id.description);
        tvChild.setText(cursor.getString(cursor
                .getColumnIndex(DatabaseHelper.colNAME)));
    }

    @Override
    protected void bindGroupView(View view, Context context, Cursor cursor,
                                 boolean isExpanded) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.group_view, null);
        }
        TextView tvGrp = (TextView) view.findViewById(R.id.textGroup);
        tvGrp.setText(cursor.getString(cursor
                .getColumnIndex(DatabaseHelper.colCityName)));
    }

    @Override
    protected Cursor getChildrenCursor(Cursor groupCursor) {
        String city = groupCursor.getString(groupCursor
                .getColumnIndex(DatabaseHelper.colCityName));
        return databaseHelper.getChildrenCursor(city);
    }

    @Override
    protected View newChildView(Context context, Cursor cursor,
                                boolean isLastChild, ViewGroup parent) {
        View mView = mInflator.inflate(R.layout.child_view, null);
        TextView tvChild = (TextView) mView
                .findViewById(R.id.description);
        tvChild.setText(cursor.getString(cursor
                .getColumnIndex(DatabaseHelper.colCityName)));
        return mView;
    }

    @Override
    protected View newGroupView(Context context, Cursor cursor,
                                boolean isExpanded, ViewGroup parent) {
        View mView = mInflator.inflate(R.layout.group_view, null);
        TextView tvGrp = (TextView) mView.findViewById(R.id.textGroup);
        tvGrp.setText(cursor.getString(cursor
                .getColumnIndex(DatabaseHelper.colCityName)));
        return mView;
    }
}
