package com.example.e.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.e.R;
import com.example.e.objects.Places;

import java.io.*;
import java.util.ArrayList;

public class PlacesListAdapter extends ArrayAdapter<Places> {

    private ArrayList<Places> placesList;
    private LayoutInflater lInflater;
    private int resourc;
    private ViewHolder holder;
  //  ImageView imgDownload;

    public PlacesListAdapter(Context context, int resource, ArrayList<Places> objects) {
        super(context, resource, objects);
        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        resourc = resource;
        placesList = objects;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        // convert view = design
        View view = convertView;
        if (view == null) {
            holder = new ViewHolder();
            view = lInflater.inflate(resourc, null);

            holder.tvCity = (TextView) view.findViewById(R.id.textViewCity);
            holder.tvText = (TextView) view.findViewById(R.id.textViewText);
            holder.tvImage = (TextView) view.findViewById(R.id.textViewImage);
            holder.tvLatitude = (TextView) view.findViewById(R.id.textViewlatitude);
            holder.tvLongtitude = (TextView) view.findViewById(R.id.textViewlongtitude);


            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.tvCity.setText(placesList.get(position).getCity().getCityName());
        holder.tvText.setText(placesList.get(position).getNameText());
        //new DownloadImageTask(imgDownload)
        //        .execute(placesList.get(position).getImageURL());
        holder.tvImage.setText(placesList.get(position).getImageURL());
        holder.tvLatitude.setText(placesList.get(position).getLatitude());
        holder.tvLongtitude.setText(placesList.get(position).getLongtitude());
        return view;

    }

    static class ViewHolder {
        public TextView tvCity;
        public TextView tvText;
        public TextView tvImage;
        public TextView tvLatitude;
        public TextView tvLongtitude;


    }

    class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;
        private static final String LOG_TAG = "puth";

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
            // String path = Environment.getExternalStorageDirectory().toString();
            OutputStream fOut = null;

            /////  File file = new File(path + "/Pictures/", "FitnessGirl" + ".jpg");
            ///  Log.d(LOG_TAG, "path = " + path);
            // получаем путь к SD
            File sdPath = Environment.getExternalStorageDirectory();
            // добавляем свой каталог к пути
            sdPath = new File(sdPath.getAbsolutePath() + "/" + "newFolder");
            // создаем каталог
            sdPath.mkdirs();
            // формируем объект File, который содержит путь к файлу
            File sdFile = new File(sdPath, "FitnessGirl" + ".jpg");

            try {
                fOut = new FileOutputStream(sdFile);
                Log.d(LOG_TAG, "sdFile = " + sdFile);
                result.compress(Bitmap.CompressFormat.JPEG, 85, fOut);

                fOut.flush();
                fOut.close();
            } catch (IOException e) {
            }
        }

    }


}


	
	

	
	

