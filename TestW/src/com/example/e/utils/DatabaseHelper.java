package com.example.e.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.*;
import android.util.Log;
import com.example.e.objects.Places;

import java.util.ArrayList;

/**
 * Created by Danila on 25.08.2014.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String LOG_TAG = "DbHelper";
    public static final String dbName = "DBplaces";//demoDB
    public static final String placesTable = "Places";//employeeTable="Employees";
    public static final String colID = "id";//"EmployeeID";
    public static final String colNAME = "name";
    public static final String colLAT = "lat";
    public static final String colLON = "lon";
    public static final String colURL = "url";
    public static final String getColCity = "idcity";
    public static final String cityTable = "city";
    public static final String colCityID = "_id";
    public static final String colCityName = "CityName";
    public static final String viewPls = "ViewPls";

    public DatabaseHelper(Context context) {
        super(context, dbName, null, 33);
    }

    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub

        db.execSQL("CREATE TABLE " + cityTable +
                "(" + colCityID + " integer primary key autoincrement, " +
                colCityName + " text unique)");

        db.execSQL("CREATE TABLE " + placesTable +
                " (" + "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                colNAME + " TEXT, " + colLAT + " TEXT, " + colLON + " TEXT, " +
                colURL + " TEXT, " + getColCity + " INTEGER" +
                " INTEGER NOT NULL ,FOREIGN KEY (" + getColCity + ") REFERENCES " +
                cityTable + " (" + colCityID + "));");

      /*  db.execSQL("CREATE TRIGGER fk_empdept_deptid " +
                " BEFORE INSERT "+
                " ON "+placesTable+

                " FOR EACH ROW BEGIN"+
                " SELECT CASE WHEN ((SELECT "+colCityID+" FROM "+
                cityTable+" WHERE "+colCityID+"=new."+colCity+" ) IS NULL)"+
                " THEN RAISE (ABORT,'Foreign Key Violation') END;"+
                "  END;");
*/
        db.execSQL("CREATE VIEW " + viewPls +
                " AS SELECT " + placesTable + "." + "_id," +
                " " + placesTable + "." + colNAME + "," +
                " " + placesTable + "." + colLAT + "," +
                " " + placesTable + "." + colLON + "," +
                " " + placesTable + "." + colURL + "," +
                " " + cityTable + "." + colCityName + "" +
                " FROM " + placesTable + " JOIN " + cityTable +
                " ON " + placesTable + "." + getColCity + " =" + cityTable + "." + colCityID);
        //проверка,есть ли в таблице city город,указанный в таблице places
     /*   db.execSQL("CREATE TRIGGER fk_empdept_deptid " +
                " BEFORE INSERT "+
                " ON "+placesTable+

                " FOR EACH ROW BEGIN"+
                " SELECT CASE WHEN ((SELECT "+colCityID+" FROM "+cityTable+
                " WHERE "+colCityID+"=new."+colCity+" ) IS NULL)"+
                " THEN RAISE (ABORT,'Foreign Key Violation') END;"+
                "  END;");*/
        //Inserts pre-defined departments
        //InsertDepts(db);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + placesTable);
        db.execSQL("DROP TABLE IF EXISTS " + cityTable);
        db.execSQL("DROP TRIGGER IF EXISTS dept_id_trigger");
        db.execSQL("DROP TRIGGER IF EXISTS dept_id_trigger22");
        db.execSQL("DROP TRIGGER IF EXISTS fk_empdept_deptid");
        db.execSQL("DROP VIEW IF EXISTS " + viewPls);
        onCreate(db);
    }

    public void InsertCity(ArrayList<Places> placesList) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();

        for (int i = 0; i < placesList.size(); i++) {
            Cursor c = db.query(cityTable, new String[]{colCityID, colCityName}, colCityName + "=?", // colCityName
                    new String[]{placesList.get(i).getCity().getCityName()}, null, null, null);

            if (c.moveToFirst() == false) {

                cv.put(colCityName, placesList.get(i).getCity().getCityName());
                long rowID = db.insert(cityTable, null, cv);

                Log.d(LOG_TAG, "row inserted city, ID = " + rowID);
            }
        }
        db.close();
    }

    public void DeletePlaces(Places pls) {
        SQLiteDatabase db = this.getWritableDatabase();
        //db.delete(placesTable, colID + "=?", new String[]{String.valueOf(pls.getId())});
        db.close();
    }

    public Cursor getAllCity() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT " + colCityID + " as _id, " +
                colCityName + " from " + cityTable, new String[]{});

        return cur;
    }

    public void getPlacesByCity(String Cit) {
        SQLiteDatabase db = this.getReadableDatabase();
        String[] columns = new String[]{"_id", colNAME, colLAT, colLON, colURL, colCityName};
        Cursor c = db.query(viewPls, columns, colCityName + "=?", new String[]{Cit}, null, null, null);
        Log.d(LOG_TAG, Integer.toString(c.getCount()));
        //c.moveToNext();
        if (c.moveToFirst()) {
            int colNqAME = c.getColumnIndex(colNAME);
            int colqLAT = c.getColumnIndex(colLAT);
            int nameCity = c.getColumnIndex(colCityName);
            do {
                // получаем значения по номерам столбцов и пишем все в лог
                Log.d(LOG_TAG,
                        "colNAME = " + c.getString(colNqAME) +
                                ", colLAT = " + c.getString(colqLAT) +
                                ", nameCity = " + c.getString(nameCity)

                );
            } while (c.moveToNext());
        }
        c.close();
        db.close();
    }

    public void readBD() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query("city", null, null, null, null, null, null);
        if (c.moveToFirst()) {
            // определяем номера столбцов по имени в выборке
            int idColIndex = c.getColumnIndex("_id");/////////////////colCityID
            int nameColIndex = c.getColumnIndex(colCityName);
            do {
                // получаем значения по номерам столбцов и пишем все в лог
                Log.d(LOG_TAG,
                        "ID = " + c.getInt(idColIndex) +
                                ", name = " + c.getString(nameColIndex)
                );
                // переход на следующую строку
                // а если следующей нет (текущая - последняя), то false - выходим из цикла
            } while (c.moveToNext());
        }
    }

    public void GetCityID(ArrayList<Places> placesList) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        for (int i = 0; i < placesList.size(); i++) {
            Cursor c = db.query(cityTable, new String[]{colCityID, colCityName}, colCityName + "=?", // colCityName
                    new String[]{placesList.get(i).getCity().getCityName()}, null, null, null);////
            c.moveToFirst();
            int a = c.getInt(c.getColumnIndex(colCityID));
            cv.put(colNAME, placesList.get(i).getNameText());
            cv.put(colLAT, placesList.get(i).getLatitude());
            cv.put(colLON, placesList.get(i).getLongtitude());
            cv.put(colURL, placesList.get(i).getImageURL());
            cv.put(getColCity, a);
            long rowID = db.insert(placesTable, null, cv);
        }
        db.close();
    }

    public void readDaB() {

        SQLiteDatabase db = this.getReadableDatabase();
        // Cursor c = db.query("places", null, null, null, null, null, null);
        Cursor c = db.query(placesTable, null, null, null, null, null, null);
        if (c.moveToFirst()) {
            // определяем номера столбцов по имени в выборке
            // int idColIndex = c.getColumnIndex(colID);
            int nameColIndex = c.getColumnIndex(colNAME);
            int latColIndex = c.getColumnIndex(colLAT);
            int lonColIndex = c.getColumnIndex(colLON);
            int urlColIndex = c.getColumnIndex(colURL);
            int forigenkey = c.getColumnIndex(getColCity);
            do {
                Log.d(LOG_TAG, "ID = " + "c.getInt(idColIndex)" +
                                ", name = " + c.getString(nameColIndex) +
                                ", lat = " + c.getString(latColIndex) +
                                // ", lon = " + c.getString(lonColIndex) +
                                // ", url = " + c.getString(urlColIndex)+
                                ", cityIDDDD = " + c.getInt(forigenkey)


                );
            } while (c.moveToNext());
        }
        c.close();
        db.close();
    }

    public Cursor getCity() {
        SQLiteDatabase db = this.getReadableDatabase();
        return db.query(cityTable, new String[]{"_id", "CityName"}, null, null, null, null, null);
    }

    public Cursor getChildrenCursor(String city) {
        SQLiteDatabase db = this.getReadableDatabase();
        String[] columns = new String[]{"_id", colNAME, colLAT, colLON, colURL, colCityName};
        Cursor c = db.query(viewPls, columns, colCityName + "=?", new String[]{city}, null, null, null);
        /*String[] columns = new String[]{"_id", colNAME, colLAT, colLON, colURL, colCityName};
        Cursor cursor = db.query(viewPls, columns, colCityID + "=?", new String[]{String.valueOf(groupId)}, null, null, null);*/
        Log.d(LOG_TAG, Integer.toString(c.getCount()));
        return c;

    }
}