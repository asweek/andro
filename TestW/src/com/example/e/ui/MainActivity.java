package com.example.e.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Toast;
import com.example.e.utils.DatabaseHelper;
import com.example.e.utils.ExpAdapter;
import com.example.e.utils.PlacesListAdapter;
import com.example.e.R;
import com.example.e.objects.City;
import com.example.e.objects.Places;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.util.ArrayList;

public class MainActivity extends Activity {

    private ArrayList<Places> placesList;
    private PlacesListAdapter adapter;
    private DatabaseHelper dbHalpr;
    private ExpAdapter expAdapter;
    private ExpandableListView expandableListView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        placesList = new ArrayList<Places>();
        new JSONAsyncTask(this).execute("http://amudo.com/places.json");
        expandableListView = (ExpandableListView) findViewById(R.id.exListView);

   /*     ListView listview = (ListView) findViewById(R.id.list);
        adapter = new PlacesListAdapter(getApplicationContext(), R.layout.row, placesList);

        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long id) {
                // TODO Auto-generated method stub
                Toast.makeText(getApplicationContext(), placesList.get(position).getCity().getCityName(), Toast.LENGTH_LONG).show();
            }
        });*/

        dbHalpr = new DatabaseHelper(this);
    }


    class JSONAsyncTask extends AsyncTask<String, Void, Boolean> {

        private ProgressDialog dialog;
        private Context context;

        JSONAsyncTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(MainActivity.this);
            dialog.setMessage("Loading, please wait");
            dialog.setTitle("Connecting server");
            dialog.show();
            dialog.setCancelable(false);
        }

        protected Boolean doInBackground(String... urls) {
            // TODO Auto-generated method stub
            try {
                HttpGet httpget = new HttpGet(urls[0]);
                HttpClient httpclient = new DefaultHttpClient();
                HttpResponse response = httpclient.execute(httpget);
                int status = response.getStatusLine().getStatusCode();

                if (status == 200) {
                    HttpEntity entity = response.getEntity();
                    String data = EntityUtils.toString(entity);

                    JSONObject jObject = new JSONObject(data);
                    JSONArray jArray = jObject.getJSONArray("places");

                    for (int i = 0; i < jArray.length(); i++) {
                        JSONObject jsonObject = jArray.getJSONObject(i);

                        Places places = new Places();
                        City city = new City();

                        places.setLatitude(jsonObject.getString("latitude"));
                        places.setLongtitude(jsonObject.getString("longtitude"));
                        //places.setCity(city.setCityName("dsd"));
                        city.setCityName(jsonObject.getString("city"));
                        places.setCity(city);
                        places.setNameText(jsonObject.getString("text"));
                        places.setImageURL(jsonObject.getString("image"));

                        placesList.add(places);
                    }
                    return true;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return false;
        }

        protected void onPostExecute(Boolean result) {
            dialog.cancel();
            //adapter.notifyDataSetChanged();
            if (result == false)
                Toast.makeText(getApplicationContext(), "Unable to fetch data from server", Toast.LENGTH_LONG).show();
            //dbHalpr.InsertCity(placesList);
            // dbHalpr.GetCityID(placesList);
            //dbHalpr.readDaB();

            Cursor cursor = dbHalpr.getCity();
            expAdapter = new ExpAdapter(cursor, context);
            expandableListView.setAdapter(expAdapter);

            dbHalpr.getPlacesByCity("Madrid");


        }
    }
}
